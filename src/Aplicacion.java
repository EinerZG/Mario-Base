import javax.swing.JFrame;
import com.einerzg.mario.Juego;

/**
 * Clase que permite iniciar la aplicación
 * 
 * @author EinerZG
 *
 */
public class Aplicacion {

	/**
	 * Inicia el juego el cual es agregado al frame creado
	 * 
	 * @param args de arranque
	 */
	public static void main(String[] args) {

		// inicializacion del juego
		Juego juego = new Juego();
		// marcosobre el cual se mostrará el juego
		JFrame marco = new JFrame(Juego.TITULO);
		// se agrega el juego al marco
		marco.add(juego);
		// se adecua el juego
		marco.pack();
		marco.setResizable(false);
		marco.setLocationRelativeTo(null);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco.setVisible(true);
		// debe ser la ultima line a usar ya que necesita de la inicializacion
		// de todos los elementos del juego
		juego.iniciarJuego();

	}

}
