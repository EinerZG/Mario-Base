package com.einerzg.mario.sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * Permite obtener una referencia a la hoja de sprites 
 * @author EinerZG
 * @version 1.0
 */
public class Spritesheets {
	
	// almacena una referencia a la hoja de sprites completa
	private BufferedImage sheet;
	// ancho y alto de cada uno de los sprites contenidos en la hoja
	private int anchoImg, altoImag;
	
	/**
	 * Inicializa la hoja de sprites
	 * @param ruta ruta de la imagen 
	 * @param anchoImg ancho en pixeles de cada sprite
	 * @param altoImag alto en pixeles de cada sprite
	 */
	public Spritesheets(String ruta, int anchoImg, int altoImag) {
		
		this.anchoImg= anchoImg;
		this.altoImag= altoImag;
		
		try {
			sheet= ImageIO.read(getClass().getResource(ruta));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * devuelve la referencia de un sprite
	 * @param x numero de sprite en x
	 * @param y número de speite en y
	 * @return sprite que se encuentra el la ubicacion indicada
	 */
	public BufferedImage getImagen(int x, int y){
		return sheet.getSubimage(x*(anchoImg), y*(altoImag), anchoImg, altoImag);
	}

	/**
	 * @return numero de tiles en x
	 */
	public int numeroTilesX(){
		return sheet.getWidth()/anchoImg;
	}
	
	/**
	 * @return numero de tiles en y
	 */
	public int numeroTilesY(){
		return sheet.getHeight()/altoImag;
	}

	
	

}
