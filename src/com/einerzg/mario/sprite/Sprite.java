package com.einerzg.mario.sprite;

import java.awt.image.BufferedImage;
/**
 * Clase para manejar los sprites
 * @author EinerZG
 * @version 1.0
 */
public class Sprite {
	
	// permite obtener la referencia de la imagen que se desea pintar
	private BufferedImage imagen;
	
	/**
	 * Inicializa la imagen que se desea pintar
	 * @param sheet hoja de sprites de la cual se obtiene la imagen
	 * @param x posicion x en la hoja de sprites
	 * @param y poscion y en la hoja de sprites
	 */
	public Sprite(Spritesheets sheet, int x, int y) {
		imagen= sheet.getImagen(x, y);
	}

	/**
	 * permite obtener una referencia del flujo que contiene la imagen
	 * @return retorna el flujo de la imagen que se desea pintar 
	 */
	public BufferedImage getImagen() {
		return imagen;
	}
	
}
