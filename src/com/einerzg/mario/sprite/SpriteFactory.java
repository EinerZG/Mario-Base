package com.einerzg.mario.sprite;

import java.util.HashMap;

/**
 * Clase encargada de optimizar el cargado de las imagenes
 * 
 * @author EinerZG
 * @version 1.0
 */
public class SpriteFactory {

	// estrucutura que guada todos los sprites a mostrar
	private HashMap<String, Sprite[]> cache;

	// instancia de la clase para apuntar a solo una referencia
	private static final SpriteFactory instancia = new SpriteFactory();

	/**
	 * inicia la lista de informacion con el arreglo de sprites
	 */
	private SpriteFactory() {
		cache = new HashMap<String, Sprite[]>();
	}

	/**
	 * Carga el sprite con base a los parametros. si ya ha cargado el sprite
	 * devuelve el inicializado anteriormente
	 * 
	 * @param nombreArchivoSprite
	 *            nombre de la hoja que contiene los sprites
	 * @param numeroSprintes
	 *            numero de sprites a cargar
	 * @param anchoSprite
	 *            ancho en pixeles del sprites
	 * @param altoSprite
	 *            alto en pixeles del sprite
	 * @return retorna el arreglo de sprites indicado
	 */
	public Sprite[] loadSprite(String nombreArchivoSprite, int numeroSprintes, int anchoSprite, int altoSprite) {

		if (cache.containsKey(nombreArchivoSprite)) {
			return cache.get(nombreArchivoSprite);
		}

		Sprite sprites[] = new Sprite[numeroSprintes];
		Spritesheets spritesheets = new Spritesheets(nombreArchivoSprite, anchoSprite, altoSprite);

		int columnas = spritesheets.numeroTilesX();
		int filas = spritesheets.numeroTilesY();

		int indice = 0;

		for (int j = 0; j < filas; j++) {
			for (int i = 0; i < columnas; i++) {

				if (indice >= numeroSprintes)
					break;

				sprites[indice] = new Sprite(spritesheets, i, j);
				indice++;

			}
		}

		cache.put(nombreArchivoSprite, sprites);

		return sprites;
	}

	/**
	 * @return the instancia
	 */
	public static SpriteFactory getInstancia() {
		return instancia;
	}

}
