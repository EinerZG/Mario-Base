package com.einerzg.mario.elemetos;

import java.awt.Graphics;

import com.einerzg.mario.Handler;

/**
 * Clase para controlar las paredes del juego
 * 
 * @author EinerZG
 * @version 1.0
 */
public class Pared extends Tile {

	// ancho en pixeles de cada uno de los tiles
	public static int TILE_MUNDO_ANCHO = 18;
	// alto en pixeles de cada uno de los tiles
	public static int TILE_MUNDO_ALTO = 18;
	// ruta de la imagen con la hoja de tiles
	public static String MUNDO = "/tileset_gutter.png";

	/**
	 * Permite inicizalir una pared con la aimgen tile
	 * @param x posicion x del jugador
	 * @param y posicion y del jugador
	 * @param alto alto del elemento en pixeles
	 * @param ancho ancho del elemento en pixeles
	 * @param solid se determina si el elemento es solido
	 * @param id identificador del personaje
	 * @param handler manejador que permite realizar operaciones (agregar, eliminar, otras)
	 */
	public Pared(int x, int y, int alto, int ancho, boolean solid, Id id, Handler handler) {
		super(x, y, alto, ancho, solid, id, handler);

		nombreArchivoSprite = MUNDO;
		anchoSprite = TILE_MUNDO_ANCHO;
		altoSprite = TILE_MUNDO_ALTO;
		numeroSprite = 1;
		numeroNivelesSprite= 1;

		init();
	}

	/**
	 * realiza un llamado al método de la superclase para que pinte el elemento
	 */
	@Override
	public void render(Graphics g) {
		super.render(g);
	}

	/**
	 * se usa en caso de tener que generar algun movimiento en el tile
	 */
	@Override
	public void tick() {
	}

}
