package com.einerzg.mario.elemetos;

import com.einerzg.mario.Handler;

/**
 * Permite manejar la información de todos los tiles en general
 * 
 * @author EinerZG
 * @version 1.0
 */
public abstract class Tile extends ElementoDeJuego {

	/**
	 * permite cargar la informacion general de cada uno de los tiles
	 * 
	 * @param x
	 *            posicion x del jugador
	 * @param y
	 *            posicion y del jugador
	 * @param alto
	 *            alto del elemento en pixeles
	 * @param ancho
	 *            ancho del elemento en pixeles
	 * @param solid
	 *            se determina si el elemento es solido
	 * @param id
	 *            identificador del personaje
	 * @param handler
	 *            manejador que permite realizar operaciones (agregar, eliminar,
	 *            otras)
	 */
	public Tile(int x, int y, int alto, int ancho, boolean solid, Id id, Handler handler) {
		super(x, y, alto, ancho, solid, id, handler);
	}

	/**
	 * elimina un tile del juego usando el hanlder
	 */
	public void morir() {

	}

}
