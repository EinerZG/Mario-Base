package com.einerzg.mario.elemetos;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.einerzg.mario.Handler;

/**
 * Permite tener el control de un personaje en particular
 * 
 * @author EinerZG
 * @version 1.0
 */
public abstract class Personaje extends ElementoDeJuego {

	// velocidad de movimiento positivo del personaje
	public static int VELOCIDAD_P = 5;
	// velocidad de movimiento negativo del personaje
	public static int VELOCIDAD_N = -5;
	// velocidad cero (cuando esta detenido)
	public static int VELOCIDAD_CERO = 0;
	// gravedad que atrae al personaje
	public static double GRAVEDAD = 9.8;


	// permite conocer si el personaje esta saltando
	protected boolean saltar;
	// permite conocer si el personaje esta cayendo
	protected boolean caer;
	// se define la gravedad que atrae al peronaje
	protected double gravedad;
	// dice cual animación se debe realizar. 0 caminar izquierda, 1 caminar
	// derecha y 2 estatico
	protected int numeroAnimacion;

	/**
	 * Permite inicliazar la iformación de los elementos de un personaje en
	 * general
	 * 
	 * @param x
	 *            posicion x del jugador
	 * @param y
	 *            posicion y del jugador
	 * @param alto
	 *            alto del elemento en pixeles
	 * @param ancho
	 *            ancho del elemento en pixeles
	 * @param solid
	 *            se determina si el elemento es solido
	 * @param id
	 *            identificador del personaje
	 * @param handler
	 *            manejador que permite realizar operaciones (agregar, eliminar,
	 *            otras)
	 */
	public Personaje(int x, int y, int alto, int ancho, boolean solid, Id id, Handler handler) {
		super(x, y, alto, ancho, solid, id, handler);
		saltar = false;
		caer = true;
		gravedad = 0.0;
		numeroAnimacion = 2;
	}

	/**
	 * permite pintar en patalla el sprite deseado
	 */
	public abstract void render(Graphics g);

	/**
	 * permite realizar el control de las coliciones y algunos movimientos
	 */
	public abstract void tick();

	/**
	 * método usado para elminar un personaje del juego
	 */
	public void morir() {
		handler.removerPersonaje(this);
	}

	/**
	 * permite modificar la animación del personaje
	 * @param anim numero de animacion a mostrar
	 */
	public void setAnimacion(int anim) {
		this.numeroAnimacion = anim;
	}
	
	/**
	 * permite detectar la colisiones por enciama del personaje
	 * @return rectanculo de colision de la parte alta del personaje 
	 */
	public Rectangle getBoundsTop() {
		return new Rectangle(x + 10, y, ancho - 20, 5);
	}

	/**
	 * permite detectar la colision en la parte baja del personaje
	 * @return rectangulo de colision de la parte baja del personaje
	 */
	public Rectangle getBoundsBottom() {
		return new Rectangle(x + 10, y + ancho - 5, ancho - 20, 5);
	}

	/**
	 * permite dectectar la colision con la izquierda del personaje
	 * @return rectangulo de colision de la derecha del personaje
	 */
	public Rectangle getBoundsLeft() {
		return new Rectangle(x, y + 10, 5, alto - 20);
	}

	/**
	 * permite dectectar la colision de la derecha del personaje
	 * @return rectangulo de colision de la derecha del persoanje
	 */
	public Rectangle getBoundsRight() {
		return new Rectangle(x + alto - 5, y + 10, 5, alto - 20);
	}

	/**
	 * @return the saltar
	 */
	public boolean isSaltar() {
		return saltar;
	}

	/**
	 * @param saltar the saltar to set
	 */
	public void setSaltar(boolean saltar) {
		this.saltar = saltar;
	}

	/**
	 * @return the caer
	 */
	public boolean isCaer() {
		return caer;
	}

	/**
	 * @param caer the caer to set
	 */
	public void setCaer(boolean caer) {
		this.caer = caer;
	}

	/**
	 * @return the gravedad
	 */
	public double getGravedad() {
		return gravedad;
	}

	/**
	 * @param gravedad the gravedad to set
	 */
	public void setGravedad(double gravedad) {
		this.gravedad = gravedad;
	}

	/**
	 * @return the numeroAnimacion
	 */
	public int getNumeroAnimacion() {
		return numeroAnimacion;
	}

	/**
	 * @param numeroAnimacion the numeroAnimacion to set
	 */
	public void setNumeroAnimacion(int numeroAnimacion) {
		this.numeroAnimacion = numeroAnimacion;
	}

	
	
}
