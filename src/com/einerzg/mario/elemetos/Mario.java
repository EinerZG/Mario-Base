package com.einerzg.mario.elemetos;

import java.awt.Graphics;
import java.awt.event.KeyEvent;

import com.einerzg.mario.Handler;
import com.einerzg.mario.Juego;
import com.einerzg.mario.KeyInputObserver;

/**
 * Esta clase permite manejar la infomación de Mario
 * 
 * @author EinerZG
 * @version 1.0
 */
public class Mario extends Personaje {

	// ruta donde se encuantran tiles de mario
	public static String MARIO_PEQUENO = "/little_mario.png";
	// número de tiles que tiene la hoja de sprites
	public static int NUMERO_SPRITES = 28;
	// ancho de la imagen .png
	public static int TILE_MARIO_ANCHO = 16;
	// alto de la imagen .png
	public static int TILE_MARIO_ALTO = 16;
	// número de frames que tienen la iamgen
	private int frame;
	// delay para realizar la animacion
	private int frameDelay;
	// dice si el personaje esta siendo animado
	private boolean animado;

	/**
	 * Permite inicizalir una mario bros
	 * 
	 * @param x
	 *            posicion x del jugador
	 * @param y
	 *            posicion y del jugador
	 * @param alto
	 *            alto del elemento en pixeles
	 * @param ancho
	 *            ancho del elemento en pixeles
	 * @param solid
	 *            se determina si el elemento es solido
	 * @param id
	 *            identificador del personaje
	 * @param handler
	 *            manejador que permite realizar operaciones (agregar, eliminar,
	 *            otras)
	 */
	public Mario(int x, int y, int alto, int ancho, boolean solid, Id id, Handler handler) {
		super(x, y, alto, ancho, solid, id, handler);

		frame = 0;
		frameDelay = 0;
		animado = false;
		nombreArchivoSprite = MARIO_PEQUENO;
		anchoSprite = TILE_MARIO_ANCHO;
		altoSprite = TILE_MARIO_ALTO;
		numeroSprite = NUMERO_SPRITES;
		numeroNivelesSprite = 2;

		handler.getKeyInputHandler().addKeyInputObserver(KeyEvent.VK_W, new Salto());
		handler.getKeyInputHandler().addKeyInputObserver(KeyEvent.VK_A, new CaminarDerecha());
		handler.getKeyInputHandler().addKeyInputObserver(KeyEvent.VK_D, new CaminarIzquierda());

		init();
	}

	/**
	 * permite pintar los personajes en en pantalla la animacion 0 que indica
	 * que se camina a la derecha la animación 1 indica que se camina a la
	 * izquierda la animación 2 indica que se queda detenido
	 * 
	 * @param g
	 *            pinxel como se pinta el personaje
	 */
	@Override
	public void render(Graphics g) {
		if (numeroAnimacion == 0) {
			g.drawImage(sprites[sprites.length / 2 + 1 + frame].getImagen(), x, y, ancho, alto, null);
		} else if (numeroAnimacion == 1) {
			g.drawImage(sprites[sprites.length / 2 - 2 - frame].getImagen(), x, y, ancho, alto, null);
		} else if (numeroAnimacion == 2) {
			g.drawImage(sprites[sprites.length / 2].getImagen(), x, y, ancho, alto, null);
		}
	}

	/**
	 * permite controlar las colisiones de mario la detención de colisiones se
	 * realiza por medio de la revicion de lo tiles de su alrededor
	 */
	@Override
	public void tick() {

		x += velX;
		y += velY;

		if (x < 0)
			x = 0;
		if (x + ancho > Juego.ANCHO * Juego.ESCALA)
			x = Juego.ANCHO * Juego.ESCALA - ancho;

		if (velX != 0)
			animado = true;
		else
			animado = false;

		for (Tile ti : handler.getTiles()) {

			if (!ti.isSolid())
				continue;

			if (ti.getId() == Id.pared) {

				if (getBoundsTop().intersects(ti.getBounds())) {
					setVelY(0);
					if (saltar) {
						saltar = true;
						gravedad = 0.0;
						caer = true;
					}
				} else if (!caer && !saltar) {
					gravedad = 0.0;
					caer = true;
				}
				if (getBoundsBottom().intersects(ti.getBounds())) {
					setVelY(0);
					y = ti.getY() - ti.getAlto();
					if (caer)
						caer = false;
				}
				if (getBoundsLeft().intersects(ti.getBounds())) {
					setVelX(0);
					x = ti.getX() + ti.getAncho();
				}
				if (getBoundsRight().intersects(ti.getBounds())) {
					setVelX(0);
					x = ti.getX() - ti.getAncho();
				}
			}

		}

		manejoDeGravedad();
		
		cambioDeAnimacion();
	}

	/**
	 * permite determinar en que momentos realizar el cambio de sprite de la
	 * animación
	 */
	private void cambioDeAnimacion() {
		if (animado) {
			frameDelay++;

			if (frameDelay >= 5) {
				frame++;
				if (frame >= 3) {
					frame = 0;
				}

				frameDelay = 0;
			}

		}
	}

	/**
	 * manejal el compartamiento de la gravedad cuando mario salta cae
	 */
	private void manejoDeGravedad() {

		if (saltar) {
			gravedad -= 0.2;
			setVelY((int) -gravedad);
			if (gravedad <= 0.0) {
				saltar = false;
				caer = true;
			}
		}

		if (caer) {
			gravedad += 0.2;
			setVelY((int) gravedad);
		}

	}

	/**
	 * clase que permite realizar el movimiento y animacion de salto
	 * 
	 * @author Christian Candela
	 */
	public class Salto implements KeyInputObserver {

		/**
		 * evento al persionar el boton
		 */
		@Override
		public void executeKeyPressed() {
			if (!isSaltar()) {
				setSaltar(true);
				setGravedad(GRAVEDAD);
			}
		}

		/**
		 * evento al soltar el botón
		 */
		@Override
		public void executeKeyReleased() {
			setVelY(VELOCIDAD_CERO);
		}

	}

	/**
	 * clase para realizar la animación y movimiento de caminar a la derecha
	 * 
	 * @author EinerZG
	 * @verion 1.0
	 */
	public class CaminarDerecha implements KeyInputObserver {

		@Override
		public void executeKeyPressed() {
			setAnimacion(1);
			setVelX(VELOCIDAD_N);
		}

		@Override
		public void executeKeyReleased() {
			setVelX(VELOCIDAD_CERO);
			setAnimacion(2);
		}
	}

	/**
	 * clase que permite realizar la animación y el movimiento de caminar a la
	 * derecha
	 * 
	 * @author EinerZG
	 * 
	 */
	public class CaminarIzquierda implements KeyInputObserver {

		@Override
		public void executeKeyPressed() {
			setAnimacion(0);
			setVelX(VELOCIDAD_P);
		}

		@Override
		public void executeKeyReleased() {
			setVelX(VELOCIDAD_CERO);
			setAnimacion(2);
		}

	}

}
