package com.einerzg.mario.elemetos;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.einerzg.mario.Handler;
import com.einerzg.mario.sprite.Sprite;
import com.einerzg.mario.sprite.SpriteFactory;
/**
 * contiene la información general de todos los elementos del juego
 * @author EinerZG
 * @version 1.0
 */
public abstract class ElementoDeJuego {
	
	/**
	 * Se crea identificadores para los tipos de elementos especificos
	 */
	public enum Id {
		jugador, pared;
	}

	//posicion x y y del elemento
	protected int x, y;
	//ancho y alto del elemento en pixeles 
	protected int alto, ancho;
	// dice si el elemento es solido o no
	private boolean solid;
	// velocidad del movimiento del elemento 
	protected int velX, velY;
	// identificador del personaje
	private Id id;
	// pemite realizar operaciones con el manejador (agregar, eliminar, etc)
	protected Handler handler;
	// arreglo de sprites que conforma el comportamiento del personaje
	protected Sprite[] sprites;
	// hoja de sprites que contiene los mosvimientos del personaje
	protected String nombreArchivoSprite;
	// ancho y alto de cada sprite
	protected int anchoSprite, altoSprite;
	// numero de sprites que se desean cargar
	protected int numeroSprite;
	// dice el numero de niveles que tiene el sprite
	protected int numeroNivelesSprite;

	/**
	 * Información para crear un elemento del juego
	 * @param x posicion x del jugador
	 * @param y posicion y del jugador
	 * @param alto alto del elemento en pixeles
	 * @param ancho ancho del elemento en pixeles
	 * @param solid se determina si el elemento es solido
	 * @param id identificador del personaje
	 * @param handler manejador que permite realizar operaciones (agregar, eliminar, otras)
	 */
	public ElementoDeJuego(int x, int y, int alto, int ancho, boolean solid, Id id, Handler handler) {
		super();
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.solid = solid;
		this.id = id;
		this.handler = handler;
	}

	/**
	 * inicializa el sprite del elemento
	 */
	public void init() {
		sprites = SpriteFactory.getInstancia().loadSprite(nombreArchivoSprite, numeroSprite, anchoSprite, altoSprite);
	}

	/**
	 * pinta el elmento sprite. Por default se muestra sprite de la posición cero   
	 * @param g elemento que permite pintar en el tablero (canvas)
	 */
	public void render(Graphics g) {
		g.drawImage(sprites[0].getImagen(), x, y, ancho, alto, null);
	}

	/**
	 * permite actualizar el comportamiento de los elementos
	 */
	public abstract void tick();

	/**
	 * se usa para eliminar el elemento
	 */
	public abstract void morir();

	/**
	 * permite generar un rectangulo del tamaño del elemento
	 * @return
	 */
	public Rectangle getBounds() {
		return new Rectangle(x, y, ancho, alto);
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the alto
	 */
	public int getAlto() {
		return alto;
	}

	/**
	 * @param alto the alto to set
	 */
	public void setAlto(int alto) {
		this.alto = alto;
	}

	/**
	 * @return the ancho
	 */
	public int getAncho() {
		return ancho;
	}

	/**
	 * @param ancho the ancho to set
	 */
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	/**
	 * @return the solid
	 */
	public boolean isSolid() {
		return solid;
	}

	/**
	 * @param solid the solid to set
	 */
	public void setSolid(boolean solid) {
		this.solid = solid;
	}

	/**
	 * @return the velX
	 */
	public int getVelX() {
		return velX;
	}

	/**
	 * @param velX the velX to set
	 */
	public void setVelX(int velX) {
		this.velX = velX;
	}

	/**
	 * @return the velY
	 */
	public int getVelY() {
		return velY;
	}

	/**
	 * @param velY the velY to set
	 */
	public void setVelY(int velY) {
		this.velY = velY;
	}

	/**
	 * @return the id
	 */
	public Id getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Id id) {
		this.id = id;
	}

	/**
	 * @return the handler
	 */
	public Handler getHandler() {
		return handler;
	}

	/**
	 * @param handler the handler to set
	 */
	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	/**
	 * @return the sprites
	 */
	public Sprite[] getSprites() {
		return sprites;
	}

	/**
	 * @param sprites the sprites to set
	 */
	public void setSprites(Sprite[] sprites) {
		this.sprites = sprites;
	}

	/**
	 * @return the nombreArchivoSprite
	 */
	public String getNombreArchivoSprite() {
		return nombreArchivoSprite;
	}

	/**
	 * @param nombreArchivoSprite the nombreArchivoSprite to set
	 */
	public void setNombreArchivoSprite(String nombreArchivoSprite) {
		this.nombreArchivoSprite = nombreArchivoSprite;
	}

	/**
	 * @return the anchoSprite
	 */
	public int getAnchoSprite() {
		return anchoSprite;
	}

	/**
	 * @param anchoSprite the anchoSprite to set
	 */
	public void setAnchoSprite(int anchoSprite) {
		this.anchoSprite = anchoSprite;
	}

	/**
	 * @return the altoSprite
	 */
	public int getAltoSprite() {
		return altoSprite;
	}

	/**
	 * @param altoSprite the altoSprite to set
	 */
	public void setAltoSprite(int altoSprite) {
		this.altoSprite = altoSprite;
	}

	/**
	 * @return the numeroSprite
	 */
	public int getNumeroSprite() {
		return numeroSprite;
	}

	/**
	 * @param numeroSprite the numeroSprite to set
	 */
	public void setNumeroSprite(int numeroSprite) {
		this.numeroSprite = numeroSprite;
	}
	
	

}
