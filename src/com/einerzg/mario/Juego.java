package com.einerzg.mario;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import com.einerzg.mario.elemetos.ElementoDeJuego.Id;
import com.einerzg.mario.elemetos.Mario;
import com.einerzg.mario.sonido.Sonido;

/**
 * Clase encargada de manejar el funcionamiento general del Juego
 * 
 * @author EinerZG
 * @version 1.0
 */
public class Juego extends Canvas {

	private static final long serialVersionUID = 1L;
	// ruta donde se encuantra el sonido de fondo
	public static String SONIDO_FONDO = "/song.wav";
	// ancho de la pantalla
	public static int ANCHO = 274;
	// alto de la apntalla
	public static int ALTO = ANCHO / 14 * 10;
	// escala del juego, a mayor resolución mayor tamaño
	public static int ESCALA = 4;
	// titulo del juego
	public static String TITULO = "Mario";
	// Hilo que controla el comportamiento del juego
	private HiloJuego hiloJuego;
	// se encarga de manejar el comportamiento de las
	// funciones generales del los elemntos de juego
	public static Handler handler;

	/**
	 * Se inicializan el marco del juego con el hilo manejador
	 */
	public Juego() {

		Dimension dimension = new Dimension(ANCHO * ESCALA, ALTO * ESCALA);
		setPreferredSize(dimension);
		setMaximumSize(dimension);
		setMinimumSize(dimension);
		hiloJuego = new HiloJuego(this);

	}

	/**
	 * En este método se agregan y pintan todos los elementos asociados con los
	 * recursos (graficos y sonidos)
	 */
	private void init() {

		handler = new Handler();

		Mario mario = new Mario(200, 400, 64, 64, true, Id.jugador, handler);
		handler.agregarPersonaje(mario);
		//addKeyListener(new KeyInput(mario));
		addKeyListener(handler.getKeyInputHandler());
		new Sonido(SONIDO_FONDO).play();

	}

	/**
	 * metodo encargado de mostrar y actualizar el movimiento de los elementos
	 * del juego
	 */
	public void render() {

		BufferStrategy bs = getBufferStrategy();

		if (bs == null) {
			createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		handler.render(g);
		g.dispose();
		bs.show();

	}

	/**
	 * Método encargado de controlar el comportamiento de los elementos del
	 * juego especificamente el del los personajes (coliciones y movimientos).
	 */
	public void tick() {
		handler.tick();
	}

	/**
	 * Permite iniciar el juego arrancando el hilo, la lectura del teclado y la
	 * parte gráfica
	 */
	public void iniciarJuego() {
		init();
		requestFocus();
		hiloJuego.iniciar();
	}

}
