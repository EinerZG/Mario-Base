package com.einerzg.mario.sonido;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sonido {

	private Clip clip;

	public Sonido(String ruta) {

		try {
			
			AudioInputStream au = AudioSystem.getAudioInputStream(getClass().getResource(ruta));
			// AudioFormat baseFormat = au.getFormat();
			// AudioFormat decodeFormat = new
			// AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
			// baseFormat.getSampleRate(), 16,
			//			baseFormat.getChannels(), baseFormat.getChannels() * 2, baseFormat.getSampleRate(), false);

			clip= AudioSystem.getClip();
			clip.open(au);
			clip.loop(Clip.LOOP_CONTINUOUSLY);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void play() {
		
		if(clip==null) return;
		stop();
		clip.setFramePosition(0);
		clip.start();

	}

	public void stop() {
		
		if(clip.isRunning()) clip.stop();
		
	}

	public void close() {
		stop();
		clip.close();
	}

}
