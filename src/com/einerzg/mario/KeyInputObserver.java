package com.einerzg.mario;
/**
 * Permite crear un marco para leer los eventos de presionar o liberar una tecla
 * @author EinerZG
 * @version 1.0
 */
public interface KeyInputObserver {
	/**
	 * se debe de agregar lo que realiza el personaje cuando se presiona una tecla
	 */
	public void executeKeyPressed();
	/**
	 * se debe de argegar lo que realiza el personaje cuando se libera una tecla
	 */
	public void executeKeyReleased();
}
