package com.einerzg.mario;

import java.awt.Graphics;
import java.util.LinkedList;

import com.einerzg.mario.elemetos.ElementoDeJuego.Id;
import com.einerzg.mario.elemetos.Pared;
import com.einerzg.mario.elemetos.Personaje;
import com.einerzg.mario.elemetos.Tile;

/**
 * Método que permite manejar las funciones basicas de los elementos del juego
 * 
 * @author EinerZG
 * @version 1.0
 */
public class Handler {

	// lista de personajes en el juego
	private LinkedList<Personaje> personajes;
	// lista de elementos tile en el juego
	private LinkedList<Tile> tiles;
	// alto en pixeles de cada elemento del juego
	public static int ALTO_TILE = 64;
	// ancho em pixeles de cada elemento del juego
	public static int ANCHO_TILE = 64;

	private KeyInputHandler keyInputHandler = new KeyInputHandler();
	/**
	 * pemite crear las lista de personajes y de tiles ademas de iniciar la
	 * creación del mundo
	 */
	public Handler() {
		personajes = new LinkedList<Personaje>();
		tiles = new LinkedList<Tile>();
		crearMundo();
	}

	/**
	 * Permite pintar y actualizar el comportamiento de los elementos del juego
	 * 
	 * @param g
	 *            pincel que pinta cada uno de los elementos
	 */
	public void render(Graphics g) {

		for (Personaje en : personajes) {
			en.render(g);
		}
		for (Tile ti : tiles) {
			ti.render(g);
		}

	}

	/**
	 * permite actualizar los movimientos del los elemementos del juego
	 */
	public void tick() {

		for (Personaje en : personajes) {
			en.tick();
		}

		for (Tile ti : tiles) {
			ti.tick();
		}
	}

	/**
	 * Se crea el mundo del juego. Aquí deberia cargarse un archivo de texto que
	 * contenga el mundo que se desea jugar, por lo cual el código contenido se debe remplazar
	 */
	public void crearMundo() {

		for (int i = 0; i < Juego.ANCHO * Juego.ESCALA / ANCHO_TILE + 1; i++) {

			Pared pared = new Pared(i * ANCHO_TILE, Juego.ALTO * Juego.ESCALA - ANCHO_TILE, ANCHO_TILE, ALTO_TILE, true,
					Id.pared, this);
			agregarTile(pared);

			if (i > 1 && i < Juego.ANCHO * Juego.ESCALA / ANCHO_TILE - 1) {

				Pared paredAlta = new Pared(i * ANCHO_TILE, 500, ANCHO_TILE, ALTO_TILE, true, Id.pared, this);
				agregarTile(paredAlta);
			}

		}
	}
	
	/**
	 * permite agregar un personaje al juego
	 * @param en personaje de que desea agregar
	 */
	public void agregarPersonaje(Personaje per) {
		personajes.add(per);
	}

	/**
	 * permite eliminar un personaje del juego
	 * @param per personaje que se desea eliminar
	 */
	public void removerPersonaje(Personaje per) {
		personajes.remove(per);
	}

	/**
	 * Obtiene la lista de personajes del juego
	 * @return lista de personajes
	 */
	public LinkedList<Personaje> getPersonajes() {
		return personajes;
	}
	
	/**
	 * permite agregar un tile al juego
	 * @param ti tile que se desea agregar
	 */
	public void agregarTile(Tile ti) {
		tiles.add(ti);
	}
 
	/**
	 * permite eliminar un tile del juego
	 * @param ti tile que se desea eliminar
	 */
	public void removerTile(Tile ti) {
		tiles.remove(ti);
	}
	
	/**
	 * Obtiene la lista de tiles del juego
	 * @return lista de tiles
	 */
	public LinkedList<Tile> getTiles() {
		return tiles;
	}

	/**
	 * @return the keyInputHandler
	 */
	public KeyInputHandler getKeyInputHandler() {
		return keyInputHandler;
	}

	/**
	 * @param keyInputHandler the keyInputHandler to set
	 */
	public void setKeyInputHandler(KeyInputHandler keyInputHandler) {
		this.keyInputHandler = keyInputHandler;
	}

	
}
