package com.einerzg.mario;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase para manejar el teclado y por ende el comportamiento de mario
 * 
 * @author EinerZG
 * @version 1.0
 */
public class KeyInputHandler  implements KeyListener   {

	private Map<Integer,List<KeyInputObserver>> keyInputObserver = new HashMap<Integer,List<KeyInputObserver>>();; 
	
	public void addKeyInputObserver(Integer key,KeyInputObserver observer){
		
		if( ! keyInputObserver.containsKey( key ) ){
			keyInputObserver.put(key, new ArrayList<KeyInputObserver>());
		} 
		List<KeyInputObserver> observers = keyInputObserver.get(key); 
		
		observers.add(observer);
	}
	
	
	public void removeKeyInputObserver(Integer key,KeyInputObserver observer){
		
		if( keyInputObserver.containsKey( key ) ){
			List<KeyInputObserver> observers = keyInputObserver.get(key); 
			observers.remove(observer);	
		} 
		
	}
	
	
	/**
	 * se activa cuando se presiona una tecla con w salta con a camina a la
	 * izquierdad con d camina a la derecha
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		Integer key = e.getKeyCode();
		if( keyInputObserver.containsKey(key) ){
			List<KeyInputObserver> observers = keyInputObserver.get(key);
			
			for (KeyInputObserver keyInputObserver : observers) {
				keyInputObserver.executeKeyPressed();
			}
			
		}
		
	}

	/**
	 * se activa cuando se deja de presionar una tecla en los tres casos de
	 * interes la velocidad se vuelve 0 y se cambia al sprite de esperar
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		Integer key = e.getKeyCode();
		if( keyInputObserver.containsKey(key) ){
			List<KeyInputObserver> observers = keyInputObserver.get(key);
			
			for (KeyInputObserver keyInputObserver : observers) {
				keyInputObserver.executeKeyReleased();
			}
			
		}
	}

	/**
	 * No es necesario para el juego
	 */
	@Override
	public void keyTyped(KeyEvent e) {
	}

}
