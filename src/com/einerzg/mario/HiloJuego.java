package com.einerzg.mario;

/**
 * Hilo encargado de dar el manejo a los elementos que se encuentran en el juego
 * 
 * @author EinerZG
 * @version 1.0
 */
public class HiloJuego implements Runnable {

	// Hilo sobre el cual se ejecuta el juego
	private Thread hilo;
	// dice si el hilo esta ejecuntadose. true para si, false para no
	private boolean ejecutandose;
	// instancia para conectarse con los elementos del juego
	private Juego juego;

	/**
	 * método que permite obtener una instacia del juego que se desea ejecutar
	 */
	public HiloJuego(Juego juego) {
		ejecutandose = false;
		this.juego = juego;
	}

	/**
	 * permite iniciar la ejecución del juego Si ya se esta ejecutando termina
	 * el método
	 */
	public synchronized void iniciar() {
		if (ejecutandose)
			return;
		ejecutandose = true;
		hilo = new Thread(this, Juego.TITULO);
		hilo.start();
	}

	/**
	 * permite detener la ejecución del juego Si ya se detuvo se termina el
	 * juego
	 */
	public synchronized void detener() {
		if (!ejecutandose)
			return;
		ejecutandose = false;
		try {
			hilo.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * método donde se actualiza el comportamiento de todos los elementos del
	 * juego. Además se adecual para que en todas las computadoras se ejecute a
	 * 60 frames por segundo.
	 */
	@Override
	public void run() {

		long ultimoTiempo = System.nanoTime();
		long minutero = System.currentTimeMillis();
		double delta = 0.0;
		double ns = 1000000000.0 / 60;
		int frames = 0;
		int ticks = 0;

		while (ejecutandose) {

			long tiempoAhora = System.nanoTime();
			delta += (tiempoAhora - ultimoTiempo) / ns;
			ultimoTiempo = tiempoAhora;

			if (delta >= 1) {
				juego.tick();
				ticks++;
				delta--;
				juego.render();
			}

			frames++;

			if (System.currentTimeMillis() - minutero > 1000) {
				minutero += 1000;
				System.out.printf("%d Frames por segungo - %d actualizacion por segundo\n", frames, ticks);
				frames = 0;
				ticks = 0;
			}

		}
		detener();
	}

}
