package com.einerzg.mario;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.einerzg.mario.elemetos.Personaje;

/**
 * Clase para manejar el teclado y por ende el comportamiento de mario
 * 
 * @author EinerZG
 * @version 1.0
 */
public class KeyInput implements KeyListener {

	// velocidad de movimiento positivo del personaje
	public static int VELOCIDAD_P = 5;
	// velocidad de movimiento negativo del personaje
	public static int VELOCIDAD_N = -5;
	// velocidad cero (cuando esta detenido)
	public static int VELOCIDAD_CERO = 0;
	// gravedad que atrae al personaje
	public static double GRAVEDAD = 9.8;
	// se maneja una instancia de mario para modificar sus
	// movimientos usando el teclado
	private Personaje mario;

	/**
	 * permite repferencia el personaje que sera controlado con el teclado
	 * 
	 * @param mario
	 *            personaje a controlar
	 */
	public KeyInput(Personaje mario) {
		this.mario = mario;
	}

	/**
	 * se activa cuando se presiona una tecla con w salta con a camina a la
	 * izquierdad con d camina a la derecha
	 */
	@Override
	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();

		switch (key) {
		case KeyEvent.VK_W:
			if (!mario.isSaltar()) {
				mario.setSaltar(true);
				mario.setGravedad(GRAVEDAD);
			}
			break;
		case KeyEvent.VK_A:
			mario.setAnimacion(1);
			mario.setVelX(VELOCIDAD_N);
			break;
		case KeyEvent.VK_D:
			mario.setAnimacion(0);
			mario.setVelX(VELOCIDAD_P);
			break;

		}

	}

	/**
	 * se activa cuando se deja de presionar una tecla en los tres casos de
	 * interes la velocidad se vuelve 0 y se cambia al sprite de esperar
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		switch (key) {
		case KeyEvent.VK_W:
			mario.setVelY(VELOCIDAD_CERO);
			break;
		case KeyEvent.VK_A:
			mario.setVelX(VELOCIDAD_CERO);
			mario.setAnimacion(2);
			break;
		case KeyEvent.VK_D:
			mario.setVelX(VELOCIDAD_CERO);
			mario.setAnimacion(2);
			break;

		}

	}

	/**
	 * No es necesario para el juego
	 */
	@Override
	public void keyTyped(KeyEvent e) {
	}

}
